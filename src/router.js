import Vue from 'vue'
import VueRouter from 'vue-router'


import ListaPropostas from './components/ListaPropostas.vue'

Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', component: ListaPropostas },
    { path: '/listaProp', component: ListaPropostas },
  ]
})